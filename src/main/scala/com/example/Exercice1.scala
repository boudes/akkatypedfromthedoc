package fr.mipn.akka.exercice1
import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.AbstractBehavior
import akka.actor.typed.scaladsl.ActorContext
import scala.util.Random

object Bob {
  def apply(): Behavior[String] = Behaviors.setup(context => new Bob(context))
}

class Bob(context: ActorContext[String]) extends AbstractBehavior[String](context) {
  context.log.info("Hello je suis un Bob!")

  override def onMessage(msg: String): Behavior[String] =
    msg match {
      case "alice" =>
        context.log.info("alice reçu")
        context.spawn(Person(),"alice")
        this
      case "oscar" =>
        context.log.info("oscar reçu")
        context.spawn(Person(),"oscar")
        this
      case s => 
        context.log.info(s"message non compris : ${s}")
        this
    }
}



object Person {
  trait Command
  final case class Salut(emetteur: ActorRef[Person.Command]) extends Command

  def apply(): Behavior[Command] =
    Behaviors.setup(context => new Person(context))
}

class Person(context: ActorContext[Person.Command]) extends AbstractBehavior[Person.Command](context) {
  context.log.info("Hello !")
  import Person._

  override def onMessage(msg: Command): Behavior[Command] =
    msg match {
      case Salut(emetteur) =>
        context.log.info("Salut {}", emetteur)
        this
    }
}


object Exercice1 extends App {
  println("Salut tout le monde")
  val lesystem = ActorSystem(Bob(), "bob")
  lesystem ! "alice"
  lesystem ! "truc bizarre"
  lesystem ! "oscar"
}
